# West Los Angeles Climate History Report

The recent fires widespread in Northern California have raised concerns throughout the state over whether or not the general air quality has been markedly impacted. Fortunately, there are sensory monitors speckled across the state by the United States Environmental Protection Agency (EPA) that can provide an answer to this and more. This study analyzed the climate data of West Los Angeles collected by the 06-037-0113 monitor to determine whether or not the area has been notably affected by the northern fires.

## Data Gathering & Cleaning
### Gathering
As aforementioned, the climate data was extracted from the EPA's 06-037-0113 monitor. The EPA maintains an [interactive map](https://epa.maps.arcgis.com/apps/webappviewer/index.html?id=5f239fd3e72f424f98ef3d5def547eb5&extent=-146.2334,13.1913,-46.3896,56.5319) of every active EPA-commissioned air quality monitor across the nation with corresponding apis to daily air quality data. 06-037-0113 contains daily data of West LA air quality dating back to 1990. The `` glue `` package was used to automate the process of retrieving data from the years 1990 to now (2020):
```{r}
### Retrieve data
year <- c(1990:2020)
ac <- 
  lapply(
    glue::glue(
      'https://www3.epa.gov/cgi-bin/broker?_service=data&_program=dataprog.Daily.sas&check=void&polname=Ozone&debug=0&year={year}&site=06-037-0113'
    ),
    fread
  )
```
### Cleaning 
The retrieved data is then compiled into a single table and cleansed of inconsistent formatting. The cleansed dataset is exported to `ac-dt-tidy.csv`.

Since the data is well categorized by EPA, a sectional visual summary can be quickly done on the data:

![](images/dt-metrics.png)

Generally, all of the metric levels of green house gases are on the decline, fortunately. Barometric pressue has also been on the upswing, meaning there should more calmer and fairer weather to look forward to in the area (in theory at least).

### Appendix I: Air Quality over Time

#### Ozone Spreads
![](images/ozone.png)

### Air Quality Index (AIQ)
![](images/aiq.png)

As depicted in the dotted gradient graphs above, air quality in West LA has generally improved since 1990 (albeit gradually). The most drastic reductions in pollution are observed from 2019 onwards, which can confidently be attributed to the recent quarantine restrictions following the COVID19 pandemic. The fires could be responsible for the outliers still present in the most recent observations, but they seem to have as outstanding of an impact in the broader context. To better study the changes in air quality over time, consider a heatmap: 

![](images/aiq-heatmap.png)

From about 2009 onwards we see the number of unhealthy spikes in air quality drop considerably, meaning that even before COVID policies the city region has been making considerable effort in improving air quality.

### Appendix II: Outdoor Temperature

![](images/outdoor-temperature.png)

Even though the monitor has been active since 1990 it appears that it hadn't recorded temperature until 2018.

### Closing

In the case of West L.A., key environmental indicators extracted from the EPA monitor suggest that the region isn't at all impacted by dramatic surrounding environmental events. In fact, it appears that the region has been on a steady and improving trend when it comes to air quality. However, that should not conclude the investigation. Which sub-regions of West L.A. are still contributing to the occassional warning levels?


